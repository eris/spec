# Quick Security Evaluation by Radically Open Security

[Radically Open Security (ROS)](https://www.radicallyopensecurity.com/) performed a quick security evaluation of ERIS as part of the services offered to NLnet projects (ERIS grew out of the [openEngiadina](https://openengiadina.net) project).

The report is availble [here](./report_ngid-openengiadina.pdf).

ROS reviewed version 0.3.0 of the specification.

Note that the review may not be considered a full audit of ERIS and neither does the review guarantee any security properties of ERIS.
