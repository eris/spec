; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules
 (eris)
 (eris read-capability)
 (eris utils base32)

 (sodium stream)
 (sodium padding)
 (sodium generichash)

 (ice-9 format)
 (ice-9 match)

 (rnrs bytevectors)
 (rnrs io ports)

 (srfi srfi-1)
 (srfi srfi-9)
 (srfi srfi-71)
 (srfi srfi-180)
 (srfi srfi-171))

;; Spec version

(define eris-spec-version "1.0.0")

;; Record type for holding test vectors

(define-record-type <eris-test-vector>
  (make-eris-test-vector
   id
   spec-version
   type
   name
   description
   content
   convergence-secret
   block-size
   read-capability
   urn
   blocks)

  eris-test-vector?

  (id eris-test-vector-id)
  (spec-version eris-test-vector-spec-version)
  (type eris-test-vector-type)
  (name eris-test-vector-name)
  (description eris-test-vector-description)

  (content eris-test-vector-content)
  (convergence-secret eris-test-vector-convergence-secret)
  (block-size eris-test-vector-block-size)

  (read-capability eris-test-vector-read-capability)
  (urn eris-test-vector-urn)
  (blocks eris-test-vector-blocks))

;; "Reducer that stores blocks into an alist of base32 encoded values
;; so that the blocks can be directly exported to JSON."
(define rtest-vector-blocks
  (case-lambda
    (() '())
    ((result) result)
    ((result ref-block)
     (cons
      (cons (string->symbol (base32-encode (car ref-block)))
            (base32-encode (cdr ref-block)))
      result))))

(define (make-positive-eris-test-vector id name description content
					convergence-secret block-size)
  (let* ((read-capability blocks (eris-encode content
					      #:block-reducer rtest-vector-blocks
					      #:block-size block-size
					      #:convergence-secret convergence-secret)))
    (make-eris-test-vector
     id
     eris-spec-version
     "positive"
     name description
     content
     convergence-secret block-size
     (->eris-read-capability read-capability)
     read-capability
     blocks)))

(define (read-capability->obj read-capability)
  `((block-size . ,(eris-read-capability-block-size read-capability))
    (level . ,(eris-read-capability-level read-capability))
    (root-reference . ,(base32-encode (eris-read-capability-root-reference read-capability)))
    (root-key . ,(base32-encode (eris-read-capability-root-key read-capability)))))

(define (obj->read-capability obj)
  (make-eris-read-capability
   (assq-ref obj 'block-size)
   (assq-ref obj 'level)
   (base32-decode (assq-ref obj 'root-reference))
   (base32-decode (assq-ref obj 'root-key))))

(define (eris-test-vector->obj test-vector)
  (filter
   (lambda (pair) (cdr pair))
   `((id . ,(eris-test-vector-id test-vector))
     (type . ,(eris-test-vector-type test-vector))
     (spec-version . ,(eris-test-vector-spec-version test-vector))
     (name . ,(eris-test-vector-name test-vector))
     (description . ,(eris-test-vector-description test-vector))
     (content . ,(if (eris-test-vector-content test-vector)
		     (base32-encode (eris-test-vector-content test-vector))
		     #f))
     (convergence-secret . ,(if (eris-test-vector-convergence-secret test-vector)
				(base32-encode (eris-test-vector-convergence-secret test-vector))
				#f))
     (block-size . ,(eris-test-vector-block-size test-vector))
     (read-capability . ,(read-capability->obj (eris-test-vector-read-capability test-vector)))
     (urn . ,(eris-test-vector-urn test-vector))
     (blocks . ,(eris-test-vector-blocks test-vector)))))

(define (obj->eris-test-vector obj)
  (make-eris-test-vector
   (assq-ref obj 'id)
   (assq-ref obj 'spec-version)
   (assq-ref obj 'type)
   (assq-ref obj 'name)
   (assq-ref obj 'description)
   (cond ((assq-ref obj 'content) => base32-decode)
	 (else #f))
   (cond ((assq-ref obj 'convergence-secret) => base32-decode)
	 (else #f))
   (assq-ref obj 'block-size)
   (obj->read-capability (assq-ref obj 'read-capability))
   (assq-ref obj 'urn)
   (assq-ref obj 'blocks)))

;;

(define null-convergence-secret (make-bytevector 32 0))
(define block-size-small 1024)
(define block-size-large (* 32 1024))
(define null-nonce (make-bytevector 12 0))

(define* (pseudo-random-bytevector length #:key (seed "Hell world!"))
  "Generate pseudo random bytevectors with ChaCha20"
  (crypto-stream-chacha20-ietf #:clen length
                               #:nonce null-nonce
                               #:key (crypto-generichash (string->utf8 seed) #:out-len 32)))

(define (test-vector-write-json-file test-vector)
  (call-with-output-file
      (format #f "test-vectors/eris-test-vector-~a-~2,'0d.json"
	      (eris-test-vector-type test-vector)
	      (eris-test-vector-id test-vector))
    (lambda (port) (json-write (eris-test-vector->obj test-vector) port))))


;;; Positive

(define (generate-positive-test-vectors)

  (test-vector-write-json-file
   (make-positive-eris-test-vector 0
				   "short string (block size 1KiB)"
				   "Encode the UTF-8 encoding of the string \"Hello world!\" with block-size 1KiB and null convergence-secret."
				   (string->utf8 "Hello world!")
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 1
				   "short string (block size 32KiB)"
				   "Encode the UTF-8 encoding of the string \"Hello world!\" with block-size 32KiB and null convergence-secret."
				   (string->utf8 "Hello world!")
				   null-convergence-secret
				   block-size-large))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 2
				   "1023 byte null string (block size 1KiB)"
				   "Encode 1023 bytes of zeroes using block-size 1KiB. This results in a single block."
				   (make-bytevector 1023 0)
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 3
				   "1024 byte null string (block size 1KiB)"
				   "Encode exactly 1KiB of zeroes using block-size 1KiB. Padding will cause an additional content block to be created."
				   (make-bytevector 1024 0)
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 4
				   "16383 byte null string (block size 1KiB)"
				   "Encode 16383 bytes of random content using block-size 1KiB."
				   (pseudo-random-bytevector 16383)
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 5
				   "16384 byte null string (block size 1KiB)"
				   "Encode 16384 bytes of random content using block-size 1KiB. This is the cutoff where 2 levels of nodes are necessary."
				   (pseudo-random-bytevector 16384)
				   null-convergence-secret
				   block-size-small))
  

  (test-vector-write-json-file
   (make-positive-eris-test-vector 6
				   "4096 byte null string (block size 1KiB)"
				   "Encode 4096 bytes of zeroes using block-size 1KiB. This results in only 3 distinct blocks as 4 content blocks have identical content and are de-duplicated."
				   (make-bytevector 4096 0)
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 7
				   "32767 byte null string (block size 32KiB)"
				   "Encode 32767 bytes of zeroes using block-size 32KiB. This results in a single block."
				   (make-bytevector 32767 0)
				   null-convergence-secret
				   block-size-large))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 8
				   "32KiB null string (block size 32KiB)"
				   "Encode exactly 32KiB of zeroes using block-size 32KiB. Padding will cause an additional block to be created."
				   (make-bytevector 32768 0)
				   null-convergence-secret
				   block-size-large))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 9
				   "short string (block size 1KiB, non-null convergence-secret)"
				   "Encode the UTF-8 encoding of the string \"Hello world!\" with block-size 1KiB and non-null convergence-secret."
				   (string->utf8 "Hello world!")
				   (pseudo-random-bytevector 32)
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 10
				   "short string (block size 32KiB, non-null convergence-secret)"
				   "Encode the UTF-8 encoding of the string \"Hello world!\" with block-size 32KiB and non-null convergence-secret."
				   (string->utf8 "Hello world!")
				   (pseudo-random-bytevector 32)
				   block-size-large))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 11
				   "1Mb of content (block size 1KiB)"
				   "Encode 1Mb of non-zero content using block-size 1KiB and null convergence-secret."
				   (pseudo-random-bytevector (* 1024 1024))
				   null-convergence-secret
				   block-size-small))
  
  (test-vector-write-json-file
   (make-positive-eris-test-vector 12
				   "1Mb of content (block size 32KiB)"
				   "Encode 1Mb of non-zero content using block-size 32KiB and null convergence-secret."
				   (pseudo-random-bytevector (* 1024 1024))
				   null-convergence-secret
				   block-size-large)))

(generate-positive-test-vectors)

;;; Negative

;; for messing up with the blocks
(define eris-test-vector-set-blocks!
  (record-modifier <eris-test-vector> 'blocks))

(define eris-test-vector-set-type!
  (record-modifier <eris-test-vector> 'type))

(define eris-test-vector-set-content!
  (record-modifier <eris-test-vector> 'content))

(define eris-test-vector-set-convergence-secret!
  (record-modifier <eris-test-vector> 'convergence-secret))

(define eris-test-vector-set-block-size!
  (record-modifier <eris-test-vector> 'block-size))

(define eris-test-vector-set-read-capability!
  (record-modifier <eris-test-vector> 'read-capability))

(define eris-test-vector-set-urn!
  (record-modifier <eris-test-vector> 'urn))

(define* (make-negative-eris-test-vector
	  id name description content convergence-secret block-size
	  #:key (f (lambda (read-capability blocks)
		     (values read-capability blocks))))

  ;; start with a positive test vector
  (define test-vector (make-positive-eris-test-vector id name description
						      content convergence-secret
						      block-size))

  ;; set type to negative
  (eris-test-vector-set-type! test-vector "negative")

  ;; unset content, convergence-secret and block-size
  (eris-test-vector-set-content! test-vector #f)
  (eris-test-vector-set-convergence-secret! test-vector #f)
  (eris-test-vector-set-block-size! test-vector #f)

  (let*
      ;; run the corrupting function
      ((read-capability blocks
			(f (eris-test-vector-read-capability test-vector)
			   (eris-test-vector-blocks test-vector))))

    ;; set the new blocks
    (eris-test-vector-set-blocks! test-vector blocks)

    ;; set the read-capability
    (eris-test-vector-set-read-capability! test-vector read-capability)

    ;; make sure URN matches read-capability
    (eris-test-vector-set-urn!
     test-vector
     (eris-read-capability->string (eris-test-vector-read-capability test-vector)))

    ;; return test vector
    test-vector))

;; Here the blocks are all missing
(test-vector-write-json-file
 (make-negative-eris-test-vector
  13
  "no blocks"
  "Content can not be decoded because of missing blocks."
  (string->utf8 "Hello world!")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values read-capability '()))))


;; Here the block has been corrupted
(test-vector-write-json-file
 (make-negative-eris-test-vector
  14
  "single invalid block"
  "Content can not be decoded because block is invalid (reference does not match content)."
  (string->utf8 "Hello world!")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values read-capability
		;; invalidate the block (there is only one)
		(map
		 (match-lambda
		   ((ref . block-b32)
		    (let* ((block (base32-decode block-b32))
			   ;; invalidate block by setting byte 42 to 0
			   (invalid-block (begin (bytevector-u8-set! block 42 0) block))
			   (invalid-block-b32 (base32-encode invalid-block)))
		      (cons ref invalid-block-b32))))
		 blocks)))))

(define (block-ref blocks ref)
  (base32-decode
   (assoc-ref blocks
	      (string->symbol (base32-encode ref)))))

(define (blake2b-256 bv)
  (crypto-generichash bv #:out-len 32))

(define* (blake2b-256-keyed bv #:key key)
  (crypto-generichash bv #:out-len 32 #:key key))

(define* (chacha20 bv #:key key nonce)
  (crypto-stream-chacha20-ietf-xor #:message bv
				   #:nonce nonce
				   #:key key))

(define (level-nonce level)
  "Returns the nonce that is used for encrypting/decrypting nodes at given level."
  (u8-list->bytevector (cons level (make-list 11 0))))

(define null-ref
  (make-bytevector 32 0))

(define (null-ref? bv)
  (bytevector=? bv null-ref))

(define (decode-node node)
  (port-transduce
   (tmap identity)
   rcons
   (lambda (port)
     (let ((ref (get-bytevector-n port 32))
	   (key (get-bytevector-n port 32)))
       ;; if eof-object or null reference
       (if (or (eof-object? ref) (eof-object? key) (null-ref? ref))
	   ;; return the eof-object
	   (eof-object)
	   ;; else return reference-key pair
	   (cons ref key))))
   (open-bytevector-input-port node)))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  15
  "single missing block "
  "Content can not be decoded because a single block (of multiple) is missing"
  (pseudo-random-bytevector
   (1- (* 4 block-size-small))
   #:seed "test-vector-negative-15")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(let* ((root-ref (eris-read-capability-root-reference read-capability))
	       (root-key (eris-read-capability-root-key read-capability))
	       (root-level (eris-read-capability-level read-capability))
	       (root-block (block-ref blocks root-ref))
	       (root-node (chacha20 root-block
				    #:key root-key
				    #:nonce (level-nonce root-level)))
	       (root-node-reference-key-pairs (decode-node root-node))
	       ;; remove the block of the fourth leaf node
	       (target-ref (car (fourth root-node-reference-key-pairs)))
	       (target-ref-symbol (string->symbol (base32-encode target-ref))))
	  (values read-capability
		  (filter
		   (match-lambda
		     ((ref . key)
		      (if (eqv? ref target-ref-symbol)
			 #f #t)))
		   blocks))))))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  16
  "single corrupted block "
  "Content can not be decoded because a single block (of multiple) is corrupted"
  (pseudo-random-bytevector
   (1- (* 5 block-size-small))
   #:seed "test-vector-negative-16")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(let* ((root-ref (eris-read-capability-root-reference read-capability))
	       (root-key (eris-read-capability-root-key read-capability))
	       (root-level (eris-read-capability-level read-capability))
	       (root-block (block-ref blocks root-ref))
	       (root-node (chacha20 root-block
				    #:key root-key
				    #:nonce (level-nonce root-level)))
	       (root-node-reference-key-pairs (decode-node root-node))
	       ;; remove the block of the fourth leaf node
	       (target-ref (car (fourth root-node-reference-key-pairs)))
	       (target-ref-symbol (string->symbol (base32-encode target-ref))))
	  (values read-capability
		  (filter-map
		   (match-lambda
		     ((ref . block)
		      (if (eqv? ref target-ref-symbol)
			  (cons ref
				(let ((block (base32-decode block)))
				  (bytevector-u8-set! block 42 0)
				  (base32-encode block)))
			  (cons ref block))))
		   blocks))))))


(test-vector-write-json-file
 (make-negative-eris-test-vector
  17
  "manipulated read capability level"
  "Level encoded in read capability has been increased. The verification of the root key in the read capability will fail as an invalid nonce was used to decode the apparent root node."
  (pseudo-random-bytevector
   64
   #:seed "test-vector-negative-17")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values
	 (make-eris-read-capability
	  (eris-read-capability-block-size read-capability)
	  (1+ (eris-read-capability-level read-capability))
	  (eris-read-capability-root-reference read-capability)
	  (eris-read-capability-root-key read-capability))
	 blocks))))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  18
  "manipulated read capability key (multiple levels)"
  "Root key encoded in read capability has been manipulated. Level of tree is larger than 0 so the root key is verified and the verification fails."
  (pseudo-random-bytevector
   (1- (* 7 block-size-small))
   #:seed "test-vector-negative-18")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values
	 (make-eris-read-capability
	  (eris-read-capability-block-size read-capability)
	  (eris-read-capability-level read-capability)
	  (eris-read-capability-root-reference read-capability)
	  (let ((key (eris-read-capability-root-key read-capability)))
	    ;; set the 18th byte to 0
	    (bytevector-u8-set! key 18 0)
	    key))
	 blocks))))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  19
  "manipulated read capability key (single levels)"
  "Root key encoded in read capability has been manipulated. Level of tree is exactly 0 so the root key is not verified. The decrypted leaf node is invalid as it has been decrypted with an invalid key. Luckily the padding is also invalid and decoding fails because of invalid padding."
  (pseudo-random-bytevector
   (1- (* 1 block-size-small))
   #:seed "test-vector-negative-19")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values
	 (make-eris-read-capability
	  (eris-read-capability-block-size read-capability)
	  (eris-read-capability-level read-capability)
	  (eris-read-capability-root-reference read-capability)
	  (let ((key (eris-read-capability-root-key read-capability)))
	    ;; set the 19th byte to 0
	    (bytevector-u8-set! key 19 0)
	    key))
	 blocks))))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  20
  "manipulated read capability block-size (increase)"
  "Block size encoded in read capability has been increased. Decoding fails because dereferenced block has invalid size."
  (pseudo-random-bytevector
   64
   #:seed "test-vector-negative-20")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(values
	 (make-eris-read-capability
	  block-size-large
	  (eris-read-capability-level read-capability)
	  (eris-read-capability-root-reference read-capability)
	  (eris-read-capability-root-key read-capability))
	 blocks))))

(test-vector-write-json-file
 (make-negative-eris-test-vector
  21
  "manipulated read capability block-size (decrease)"
  "Block size encoded in read capability has been decreased. Decoding fails because dereferenced block has invalid size."
  (pseudo-random-bytevector
   (1- block-size-large)
   #:seed "test-vector-negative-21")
  null-convergence-secret
  block-size-large
  #:f (lambda (read-capability blocks)
	(values
	 (make-eris-read-capability
	  block-size-small
	  (eris-read-capability-level read-capability)
	  (eris-read-capability-root-reference read-capability)
	  (eris-read-capability-root-key read-capability))
	 blocks))))

;; Test for no/invalid padding. This requires almost manual encodig...

(test-vector-write-json-file
 (let* ( ;; Use some random bytes as leaf node (no padding!)
	(leaf-node (pseudo-random-bytevector
		    block-size-small
		    #:seed "test-vector-negative-22"))
	(key (blake2b-256-keyed leaf-node
				#:key null-convergence-secret))
	(block (chacha20 leaf-node
			 #:key key
			 #:nonce (level-nonce 0)))
	(ref (blake2b-256 block))
	(read-capability (make-eris-read-capability block-size-small 0 ref key)))

   (make-eris-test-vector
    22
    eris-spec-version
    "negative"
    "no padding"
    "Content has not been padded (but has exactly size block-size). Decoding fails because of invalid/no padding."
    #f
    #f
    #f
    read-capability
    (eris-read-capability->string read-capability)
    `((,(string->symbol (base32-encode ref)) . ,(base32-encode block))))))

(test-vector-write-json-file
 (let* ( ;; some random content (smaller than block size)
	(content (pseudo-random-bytevector
		  (- block-size-small 128)
		  #:seed "test-vector-negative-23"))
	;; pad content to make leaf node
	(leaf-node (sodium-pad content #:block-size block-size-small))
	;; manipulate padding by setting a byte that should be 0x00 to 0x42
	(_ (bytevector-u8-set! leaf-node
			       ;; set the 23rd byte from right to 0x42
			       (- block-size-small 23) #x42))
	(key (blake2b-256-keyed leaf-node
				#:key null-convergence-secret))
	(block (chacha20 leaf-node
			 #:key key
			 #:nonce (level-nonce 0)))
	(ref (blake2b-256 block))
	(read-capability (make-eris-read-capability block-size-small 0 ref key)))

   (make-eris-test-vector
    23
    eris-spec-version
    "negative"
    "invalid padding"
    "Content has been padded incorrectly. Decoding fails because of invalid padding."
    #f
    #f
    #f
    read-capability
    (eris-read-capability->string read-capability)
    `((,(string->symbol (base32-encode ref)) . ,(base32-encode block))))))


;; Test for invalid padding of internal nodes

(test-vector-write-json-file
 (make-negative-eris-test-vector
  24
  "invalid internal node"
  "An internal node has random content inserted instead of null reference-key pairs. Decoding fails as the invalid internal node is detected."
  ;; some random content that fits in two leaf nodes
  (pseudo-random-bytevector
   (1- (* 2 block-size-small))
   #:seed "test-vector-negative-24")
  null-convergence-secret
  block-size-small
  #:f (lambda (read-capability blocks)
	(let* ((root-ref (eris-read-capability-root-reference read-capability))
	       (root-key (eris-read-capability-root-key read-capability))
	       (root-level (eris-read-capability-level read-capability))
	       (root-block (block-ref blocks root-ref))
	       (root-node (chacha20 root-block
				    #:key root-key
				    #:nonce (level-nonce root-level)))
	       ;; decode the two reference key pairs from root-node
	       (rk-pairs (decode-node root-node))

	       ;; create a port to construct the corrupt root-node
	       (port get-corrupt-root-node (open-bytevector-output-port))

	       ;; construct the corrupted node
	       (corrupted-node (begin
				 ;; add the reference-key pairs to node
				 (for-each
				  (match-lambda
				    ((ref . key)
				     (put-bytevector port ref)
				     (put-bytevector port key)))
				  rk-pairs)

				 ;; add a null reference-key pair
				 (put-bytevector port (make-bytevector 32 0))
				 (put-bytevector port (make-bytevector 32 0))

				 ;; add random gibberish (this is invalid)
				 (put-bytevector port
						 (pseudo-random-bytevector
						  ;; arity is 16 and there are already 2
						  ;; rk-pairs in node
						  ;; as well as the null rk-pair.
						  (* 64 (- 16 2 1))
						  #:seed "an invalid internal node"))

				 ;; get the corrupted node
				 (get-corrupt-root-node)))

	       ;; encrypt node to block
	       (root-key' (blake2b-256 corrupted-node))
	       (root-block' (chacha20 corrupted-node
				      #:key root-key'
				      #:nonce (level-nonce root-level)))
	       (root-ref' (blake2b-256 root-block')))
	  (values
	   (make-eris-read-capability block-size-small root-level root-ref' root-key')
	   ;; add the corrupted root block to the blocks
	   (cons `(,(string->symbol (base32-encode root-ref')) .
		   ,(base32-encode root-block'))
		 blocks))))))
