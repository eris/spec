#!/bin/sh
set -e

cat << END
ErisTest subclass: #ErisSpecificationTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ERIS'
!

!ErisSpecificationTest methodsFor:'tests'!
END

for file in $@; do

cat << END

test$(jq --raw-output '.id' < $file)
	$(jq '.description' < $file | sed -e 's/\\"/""/g' -e 's/!/!!/g')
END

echo -n "	self runJsonTest: '"
sed -e "s/'/''/g" -e "s/!/!!/g" < $file
echo "'"
echo -n "!"
done

echo " !"
