# Encoding for Robust Immutable Storage (ERIS)

ERIS is an encoding for arbitrary content into uniformly sized encrypted blocks that can be reassembled only in possession of a short read capability that can be encoded as an URN.

This repository contains the specification documents.

The latest version of the specification is published at: [http://purl.org/eris](http://purl.org/eris). Please use this URL as a stable reference to the specification.

## Status

ERIS is considered to be stable. See also the [section on versioning in the specification document](https://purl.org/eris#name-versioning).

## Implementations

A list of known implementations of ERIS:

- [guile-eris](http://inqlab.net/git/guile-eris.git): The reference implementation in Guile Scheme.
- [elixir-eris](http://gitlab.com/openengiadina/elixir-eris): Elixir implementation.
- [eris](https://github.com/cjslep/eris): Go implementation.
- [eris](https://git.sr.ht/~ehmry/eris): Nim implementation.
- [js-eris](https://inqlab.net/git/js-eris.git): Javascript implementation.

See also the section "Implementations" of the specification document.

## Working on the Specification Document

The source code of the speficiation document is in the [eris.xml](./eris.xml) file. It is formatted in the IETF XML Format specified in [RFC 7991](https://datatracker.ietf.org/doc/html/rfc7991).

The [xml2rfc](https://xml2rfc.tools.ietf.org/) tool is used to generate HTML documents from the source specification.

To build the specification:

```
guix shell -D -f guix.scm -- make
```

This will produce the files `public/index.html`, `public/eris.txt` and `public/eris-test-vectors.tar.gz`.

### Schema files

RelaxNG schema files are provided for your favorite XML editor.

### Citation Library

The IETF provides a tool for getting suitable references for DOIs and RFCs. For example:

```
curl https://xml2rfc.tools.ietf.org/public/rfc/bibxml-doi/reference.DOI.10.3233/SW-190380.xml
```

or:

```
curl https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7929.xml
```

### Generate test-vectors

```
make generate-test-vectors
```

### Making a release

- Update test vectors (in particular make sure that the `spec-version` field is correct)
- Add changelog entry
- Adapt link to test vectors archive in section `Test Vectors` to a versioned archive (e.g. `eris-test-vectors-vX.X.X.tar.gz`).
- Copy `public/index.html` to `public/eris-vX.X.X.html`
- Copy `public/eris-test-vectors.tar.gz` to `public/eris-test-vectors-vX.X.X.tar.gz`
- Manually adapt the generated HTML (see below)
- Create a release commit
- Tag the commit with an annotated and signed tag (e.g. `git tag -s v0.4.0 -m "0.4.0"`)
- Push to repository at codeberg
- Publish the html to the `pages` branch.

#### Manually adapt the generated HTML

The IETF `xml2rfc` tool adds some things that we do not want in the final document: Expiration date, and copyright boilerplate. Currently we just edit the generated HTML by hand to remove such things. Things that should be manually adapted in generated HTML before releasing:

- Remove expiration date
- Remove working group
- Remove intended status
- Add a version field in the description list `identifiers`
- Remove "Status of this Memo" section (div with id `status-of-memo`)
- Remove "Copyright Notice" section (div with id `copyright`)

## Acknowledgments

ERIS was initially developed as part of the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLNet Foundation](https:/nlnet) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/). Further development is being supported by the NLnet Foundation trough [NGI Assure](https://nlnet.nl/assure/).

## Contact

Questions and comments may be directed to the [~pukkamustard/eris@sr.ht](https://lists.sr.ht/~pukkamustard/eris) mailing list or directly to the authors (pukkamustard [at] posteo.net, ehmry [at] posteo.net, dvn [at] mail.me).

You are also invited to share your implementations and use-cases on the mailing list.

Urgent and sensitive security issues may be addressed directly to the ERIS maintainers. See [security.txt](./security.txt).

## License

- Specification: [CC-BY-SA-4.0](./LICENSES/CC-BY-SA-4.0.txt)
- Test vectors: [CC0-1.0](./LICENSES/CC0-1.0.txt)
