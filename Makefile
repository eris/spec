# Makefile for building ERIS specification

SPEC = public/index.html public/eris.txt
TEST_VECTORS = public/eris-test-vectors.tar.gz

.PHONY: all
all: $(SPEC) $(TEST_VECTORS)

# Artwork

ARTWORK_PIC = $(wildcard artwork/*.pic)
ARTWORK = $(ARTWORK_PIC:%.pic=%.svg)

artwork/%.svg: artwork/%.pic
	pikchr --svg-only $< > $@

# Test vectors

.PHONY: generate-test-vectors
generate-test-vectors: scripts/generate-test-vectors.scm
	guile scripts/generate-test-vectors.scm

$(TEST_VECTORS): test-vectors/*.json test-vectors/README
	tar cfz $@ test-vectors/

# SmallTalk test files
public/eris-test-vectors.st: test-vectors/*.json
	sh scripts/generate-smalltalk-tests.sh $^ > $@

# Specification document

public/index.html: eris.xml style.css $(ARTWORK)
	mkdir -p public
	xml2rfc -o $@ --html --no-network --css style.css $<

public/eris.txt: eris.xml $(ARTWORK)
	mkdir -p public
	xml2rfc --text --no-network -o $@ $<

